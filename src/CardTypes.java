import java.util.ArrayList;
import java.util.List;

public class CardTypes {

    public List<String> listCardTypes(){
        List<String> cardTypes = new ArrayList<>();
        cardTypes.add("Hearts");
        cardTypes.add("Spades");
        cardTypes.add("Diamonds");
        cardTypes.add("Flowers");
        return (cardTypes);
    }

}
