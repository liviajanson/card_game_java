import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Game {

    public void fillAllTypes(List<String> cardTypes, List<Card> cardsList, Random randomGenerator) {
        int placedCards = 0;
        for (String cardType : cardTypes) {

            placedCards = fillEmptySlots(cardType, randomGenerator, cardsList, placedCards);
        }
    }

    public int fillEmptySlots(String cardType, Random randomGenerator, List<Card> cardsList, int placedCards) {

        for (int i = 0; i < 12; i++) {
            Card card = new Card(cardType, i);
            int emptySlots = 0;

            int randomInt = randomGenerator.nextInt(48 - placedCards);
            placedCards = placeCard(randomInt, emptySlots, placedCards, cardsList, card);
        }
        return placedCards;
    }

    public int placeCard(int randomInt, int emptySlots, int placedCards, List<Card> cardsList,  Card card) {

        for (int b = 0; b < 48; b++) {

            if (cardsList.get(b) != null) {
                continue;
            }

            if (emptySlots == randomInt) {
                cardsList.set(b, card);
                //System.out.println(card + "\t" + b + "\t"+ randomInt + "\t" + cardsList);
                placedCards++;
                break;
            }
            emptySlots++;
        }
        return placedCards;
    }


    public List<Card> stackCards() {//segatud kaardid?
        Random randomGenerator = new Random();

        CardTypes fromCardTypes = new CardTypes();
        List<String> cardTypes = fromCardTypes.listCardTypes();
        List<Card> cardsList = new ArrayList(Arrays.asList(new Card[48]));

        fillAllTypes(cardTypes, cardsList, randomGenerator);


        return cardsList;
    }
    public Card dealACardToPlay(List<Card> cardInHands, Card cardOnTheTable) {
        Card bestSoFar = null;
        for (Card card : cardInHands) {
            if (card.number < cardOnTheTable.number) {//vordlen kaarte
                continue;
            }//kui see ylemine tosi, siis l'heb siia alla
            if (bestSoFar==null){
                bestSoFar=card;
                continue;
            }
            if (card.number > bestSoFar.number) {//vordlen valja valitud kaardiga teisi
                continue;
            }
            bestSoFar = card;
        }
        return bestSoFar;
    }
    public Card smallestCard(List<Card> cardInHands){
        Card smallestSoFar = null;
        for (Card card : cardInHands){
            if (smallestSoFar==null){
                smallestSoFar=card;
                continue;
            }
            if(card.number<smallestSoFar.number){
                smallestSoFar=card;
            }
        }
        return smallestSoFar;
    }
}
