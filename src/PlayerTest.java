import java.util.ArrayList;
import java.util.List;

public class PlayerTest {

    public void testSumOfPlayerStack(){
        List<Card> player1Stack= new ArrayList<>();
        player1Stack.add(new Card("Spades", 3));
        player1Stack.add(new Card("Hearts", 5));
        player1Stack.add(new Card ("Diamonds", 7));

        Player player = new Player();
        player.playerStack=player1Stack;
        int sumOfStack = player.sumOfplayerStack();
        System.out.println(sumOfStack);
    }

}
