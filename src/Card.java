public class Card {

    String figure;
    int number;



    public Card(String figure, int number) {
        this.figure = figure;
        this.number = number;
    }



    public String toString() {
        return figure + number;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Card) {
            Card card = (Card) obj;
            if (this.number == card.number && this.figure.equals(card.figure)) {//equals for string
                return true;
            }
        }
        return false;

    }

}