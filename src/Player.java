import java.util.ArrayList;
import java.util.List;

public class Player {

    List<Card> playerStack = new ArrayList<>();

    List<Card> playerCardsInHand = new ArrayList<>(3);

    public int sumOfplayerStack() {
        int total = 0;

        for (Card card : playerStack) {

            total = total + card.number;
        }
        return total;
    }
    /*List<Card> playerStack = new ArrayList<>();

    Integer integerSum = playerStack.stream().mapToInteger(Card::getNumber).sum();
        System.out.println("summation: " + integerSum);*/

}
