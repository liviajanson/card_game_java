import java.util.ArrayList;
import java.util.List;

public class GameTest {

    public void testSmallestCard(){
        List<Card> player1Cards= new ArrayList<>();
        player1Cards.add(new Card("Spades", 3));
        player1Cards.add(new Card("Hearts", 5));
        player1Cards.add(new Card ("Diamonds", 7));

        Game game = new Game ();
        Card smallestCard = game.smallestCard(player1Cards);//smallest card returned in created variable
        System.out.println(smallestCard);
    }

    public void testDealACardToPLay(){
        {
            List<Card> player1Cards= new ArrayList<>();
            player1Cards.add(new Card("Spades", 3));
            player1Cards.add(new Card("Hearts", 5));
            player1Cards.add(new Card ("Diamonds", 7));

            Card cardOnTheTable = new Card("Hearts", 4);

            Game game = new Game ();
            Card cardToPlay = game.dealACardToPlay(player1Cards,cardOnTheTable);//smallest card returned in created variable
            System.out.println(cardToPlay);



        }
    }


    public List<Card> listCardsInOrder(List<String> cardTypes) { //meetod teeb tyhja kaardilisti
        List<Card> cardListInOrder = new ArrayList<>();
        for (String cardType : cardTypes) {
            for (int i = 0; i < 12; i++) {
                Card card = new Card(cardType, i);
                cardListInOrder.add(card);
            }
        }
        return cardListInOrder;
    }

    public void compareCards(List<Card> cardListInOrder, List<Card> cardListRandom) {
        for (Card card : cardListInOrder) {
            for (int i = 0; i < 48; i++) {
                Card otherCard = cardListRandom.get(i);
                if (card.equals(otherCard)) {
                    cardListRandom.set(i, null);

                }
            }
        }
    }

    public void testCardShuffle() {

        CardTypes fromCardTypes = new CardTypes();//kutsub valja meetodi
        List<String> cardTypes = fromCardTypes.listCardTypes();
        List<Card> cardListInOrder = listCardsInOrder(cardTypes);
        System.out.println(cardListInOrder);

        Game game = new Game();//kutsub valja meetodi stackCards Game klassist
        List<Card> cardListRandom = game.stackCards();

        System.out.println(cardListRandom);

        compareCards(cardListInOrder, cardListRandom);

        boolean failed = false;
        for (Card card : cardListRandom) {
            if (card != null) {
                System.out.println("test failed");

                failed = true;
                break;
            }
        }
        if (failed == false) {
            System.out.println("success");
        }

    }

    public int ruut(int b) {
        int a = b * b;
        System.out.println(a);
        return a;
    }

}